addpath(genpath('.'));
imgsetPath = '/home/bdai/WorkSpace/DB/datasets/PASCAL VOC2007/train/ImageSets/Main/%s.txt';
imgPath = '/home/bdai/WorkSpace/DB/datasets/PASCAL VOC2007/train/JPEGImages/';
annoPath = '/home/bdai/WorkSpace/DB/datasets/PASCAL VOC2007/train/Annotations/%s.xml';
class = 'person';
[idx, ~] = textread(sprintf(imgsetPath, 'trainval'), '%s %d');
pos = [];
neg = [];
numPos = 0;
numNeg = 0;

n = numel(idx);
for i = 1 : n
    rec = PASreadrecord(sprintf(annoPath, idx{i}));
    m = length(rec.objects);
    negative = true;
    for j = 1 : m
        if (strcmp(rec.objects(j).class, class) == 1)
            negative = false;
            if (rec.objects(j).difficult)
                continue;
            end
            numPos = numPos + 1;
            pos(numPos).fullPath = imgPath;
            pos(numPos).fileName = idx{i};
            pos(numPos).ext = 'jpg';
            pos(numPos).label = 1;
            bb = rec.objects(j).bbox;
            pos(numPos).gtBB = [bb([1 2]) bb([3 4]) - bb([1 2]) + [1 1]];
        end
    end
    if (negative) 
        numNeg = numNeg + 1;
        neg(numNeg).fullPath = imgPath;
        neg(numNeg).fileName = idx{i};
        neg(numNeg).ext = 'jpg';
        neg(numNeg).label = -1;
    end
end

save(['voc_' class '.mat'], 'pos', 'neg');