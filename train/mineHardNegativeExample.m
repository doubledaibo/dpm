function latVars = mineHardNegativeExample(neg, model, para, maxSize, negIdx)
%% mine hard negative examples 
%% 
% neg: negative example image, negative examples are dense sampled from the image
% model: model
% para: parameter
% maxSize: maximum number of generated negative examples
% negIdx: index of the negative image

    para.detect.threshold = -1 - 0.5;
    dets = detect(neg, [], model, para);
    numDets = min(maxSize, length(dets));
    if (numDets > 0)
        [~, idx] = sort([dets.score], 2, 'descend');
        latVars = repmat(struct('label', 1, 'idx', 0, 'feat', []), numDets, 1);
        for i = 1 : numDets
            latVars(i).label = -1;
            latVars(i).idx = negIdx;
            latVars(i).feat = vectorizeFeature(model, neg, dets(idx(i)), para);
        end
    end
    if (~exist('latVars', 'var'))
        latVars = [];
    end
end