function [latVars, numLatVars] = removeEasyExample(latVars, numLatVars, scores)    
%% remove easy negative examples, score < -1 -0.5
% and remove redundant positive examples with lower scores
%% 
% latVars: hard examples' latent variables
% numLatVars: numel(latVars);
% scores: LSVM score of each hard example
    
    %remove easy
    idx = find(scores < -1 - 0.5);
    latVars(idx) = [];
    scores(idx) = [];
    numLatVars = numLatVars - length(idx);
    %remove redundant 
    remove = zeros(numLatVars, 1);
    idx = find([latVars.label] == 1);
    remove(idx) = 1;
    scores = scores(idx);
    exampleIdx = [latVars(idx).idx];
    [exampleIdx, sortIdx] = sort(exampleIdx);
    idx = idx(sortIdx);    
    scores = scores(sortIdx);
    n = length(exampleIdx);
    last = 1; now = 2;
    while (last <= n)
        while (now <= n && exampleIdx(last) == exampleIdx(now))
            now = now + 1;
        end
        [~, i] = max(scores(last : now - 1));
        remove(idx(last + i - 1)) = 0;
        last = now; now = now + 1;
    end
    latVars(remove == 1) = [];
    numLatVars = numLatVars - sum(remove);
end