function latVars = relabelPositiveExample(pos, model, para, maxSize)
%% acquire the highest scoring hidden label for each positive example
%%
% pos: positive examples
% model: current model
% para: parameter

    numPos = numel(pos);
    para.detect.threshold = 0 + 0.001;
    now = 0;
    for i = 1 : numPos
        det = detect(pos(i), pos(i).gtBB, model, para);
        %find the best one with significant overlap with ground-truth
        if (~isempty(det))
            now = now + 1;
            latVars(now, 1).feat = vectorizeFeature(model, pos(i), det, para);
            latVars(now).idx = i;
            latVars(now).label = 1;
        end
        if (now == maxSize)
            break;
        end
        % flip example
        example = pos(i);
        example.flip = 1;
        det = detect(example, pos(i).gtBB, model, para);
        if (~isempty(det))
            now = now + 1;
            latVars(now, 1).feat = vectorizeFeature(model, example, det, para);
            latVars(now).idx = i;
            latVars(now).label = 1;
        end
        if (now == maxSize)
            break;
        end
    end 
    if (~exist('latVars', 'var'))
        latVars = [];
    end
end