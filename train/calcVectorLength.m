function model = calcVectorLength(model)
%% compute length of each component of the model after vectorization
    n = model.numComponent;
    for i = 1 : n
        len = numel(model.components(i).rootFilter);
        len = len + 4 * model.components(i).numParts;
        j = 1; 
        while (j <= model.components(i).numParts)
            part = model.components(i).parts(j);
            if (part.partner == 1)
                %have a symmetric partner
                len = len + numel(part.filter);
                j = j + 1;
            else
                %self-symmetric
                [height, width, l] = size(part.filter);
                width = ceil(width / 2);
                len = len + height * width * l;
                j = j + 1;
            end
        end
        len = len + 1;
        model.components(i).vectorLength = len;
    end
end