function component = addParts(component, para)
%% add part model for the component
% greedily cover high energy part of root filter

    weight = imresize(component.rootFilter, 2, 'bicubic');
    energy = sum(max(weight, 0) .^ 2, 3);
    % force symmetric
    energy = energy + fliplr(energy);
    
    numParts = para.numParts;
    % pick part sizes 
    [h, w, ~] = size(weight);
    area = h * w / numParts;
    
    % possible part shapes
    k = 0;
    for ph = 3 : 1 : h - 2
        for pw = ph - 4 : ph + 4
            if (pw * ph <= area * 1.2 && pw * ph >= area)
                k = k + 1;
                template{k} = fspecial('average', [ph pw]);
            end
        end
    end
   
    % picking parts: at each iteration we pick a pair of symmetric parts
    % or a part in the center of the template
    added = 0;
    parts = repmat(struct('partner', 0, 'filter', [], 'v', [], 'd', []), numParts, 1);
    while (added < numParts)
        v = zeros(k, 1);
        x = zeros(k, 1);
        y = zeros(k, 1);
        center = floor(w / 2);
        for i = 1 : k
            score = conv2(energy, template{i}, 'valid');
            temp = score;
            % zero the half side, and zero the part that overlaps symmetric
            % partner

            z = max(center - size(template{i}, 2) + 1, 1);
            score(:, z : end) = -inf;
            
            if (added == numParts - 1)
                score(:, :) = -inf;
            end
            
            % add centered parts
            m = floor(size(template{i}, 2) / 2);
            score(:, center - m + 1) = temp(:, center - m + 1);
            
            % pick the best position for this shape
            [temp, Y] = max(score);
            [v(i), x(i)] = max(temp);
            y(i) = Y(x(i));
        end
        % pick the best
        [~, idx] = max(v);
        
        if (x(idx) <= center - floor(size(template{idx}, 2) / 2))
            partner = 1;
        else
            partner = 0;
        end
        
        added = added + 1;
        [parts(added), energy] = generatePart(weight, energy, template{idx}, x(idx), y(idx), partner);
        
        if (partner == 1)
            added = added + 1;
            [parts(added), energy] = generatePart(weight, energy, template{idx}, w - x(idx) - size(template{idx}, 2) + 2, y(idx), partner);
        end
    end
    component.parts = parts;
    component.numParts = numParts;
end

function [part, energy] = generatePart(filter, energy, template, x, y, partner)
    [h, w] = size(template);
    part.partner = partner;
    part.filter = filter(y : y + h - 1, x : x + w - 1, :); %root-filter is symmetric, no need for special care of self-symmetric
    
    part.v = [x - 1 y - 1]';
    part.d = [0 0 0.1 0.1]';
    
    %zero covered energy
    for xx = x : x + w - 1
        for yy = y : y + h - 1
            energy(yy,xx) = 0;
        end
    end
end