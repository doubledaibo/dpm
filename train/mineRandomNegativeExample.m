function latVars = mineRandomNegativeExample(neg, model, para, maxSize)
%% mine random negative example
% usually model has only one component in this situation
%%
% maxSize: maximum number of negative examples that can produce

    n = numel(neg);
    numEach = floor(maxSize / n);
    [h, w, v] = size(model.components(1).rootFilter);
    latVars = repmat(struct('label', 0, 'idx', 0, 'feat', []), numEach * n, 1);
    now = 0;
    for i = 1 : n
        [feat, ~] = getFeatPyramid(neg(i), para);
        m = numel(feat);
        for j = 1 : numEach
            sidx = randi(m, 1, 1);
            [y, x, ~] = size(feat{sidx});
            while (y < h || x < w) 
                sidx = randi(m, 1, 1);
                [y, x, ~] = size(feat{sidx});
            end
            yidx = randi(y - h + 1, 1, 1);
            xidx = randi(x - w + 1, 1, 1);
            now = now + 1;
            latVars(now).label = -1;
            latVars(now).idx = i;
            latVars(now).feat = [reshape(feat{sidx}(yidx : yidx + h - 1, xidx : xidx + w - 1, :), [1, h * w * v]) 1]; %add bias term 
        end
    end
end