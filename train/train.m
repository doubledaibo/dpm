function model = train(para, pos, neg)
%% train a DPM model
%%
% para: parameter
% pos: positive examples
% neg: negative examples

    % initialization
    % split positive examples into para.init.numGroup groups according to
    % aspects ratio
    % each train a model
    posGroups = groupPositiveExample(pos, para.train.init.numGroup);
    n = numel(posGroups);
    models = cell(n, 1);
    for i = 1 : n
        fprintf('initializing %d th component\n', i);
        models{i} = initModel(para, posGroups{i}, neg);
    end
    %merge into one mixture model
    model.numComponent = n;
    for i = model.numComponent : -1 : 1
        model.components(i, 1) = models{i}.components(1);
    end
    save('initModel.mat', 'model');
    %train using latent detections & hard negatives
    fprintf('train root filter using SVM\n');
    model = trainLSVM(para, model, pos, neg(1 : para.train.init.initNegSize), false, false);
    save('initLSVMModel.mat', 'model');
    %add parts
    for i = 1 : n
        fprintf('add parts for %d th component\n', i);
        model.components(i) = addParts(model.components(i), para);
    end
    model = calcVectorLength(model);
    save('initaddpartmodel.mat', 'model');
    %train a dpm using the initialization
    fprintf('train final model\n');
    model = trainLSVM(para, model, pos, neg, false, false);
    save('finalmodel.mat', 'model');
end

