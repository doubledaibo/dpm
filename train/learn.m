function [model, scores] = learn(model, para, latVars, pos, neg)
%% vectorize the model and learn a LSVM
%%
% model: model
% para: parameter
% latVars: latent variables of hard examples
% pos: positive examples
% neg: negative examples
    fprintf('vectorizing model\n');
    %vectorize model
    beta = vectorizeModel(model);
    %vectorisze feature
    numX = numel(latVars);
    X = zeros(numX, length(latVars(1).feat));
    for i = 1 : numX
        X(i, :) = latVars(i).feat;
    end
    Y = [[latVars.label]' [latVars.idx]'];
    %calculate lowerbound from para
    lowerbound = ones(sum([model.components.vectorLength]), 1) * para.train.filterLowerbound;
    offset = 0;
    for i = 1 : model.numComponent
        extra = numel(model.components(i).rootFilter);
        lowerbound(offset + extra + 1 : offset + extra + 4 * model.components(i).numParts) = para.train.deformLowerbound;
        offset = offset + model.components(i).vectorLength;
        lowerbound(offset) = para.train.biasLowerbound;
    end
    %gradientDescent
    fprintf('gradient descending\n');
    beta = gradientDescent(para, beta, X, Y, lowerbound);
    scores = X * beta;
    %parse model
    fprintf('parsing model\n');
    model = parseModel(model, beta);
end