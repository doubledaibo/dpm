function model = initModel(para, pos, neg)
%% get a good initialization
%% 
% para: parameter
% pos: positive examples
% neg: negative examples
if (isfield(para.train.init, 'size'))
    size = para.train.init.size;
else
    %pick mode of aspect ratios
    gt = [pos.gtBB];
    idx = 3 : 4 : numel(gt);
    w = gt(idx);
    idx = idx + 1;
    h = gt(idx);
    filter = exp(-[-100: 100].^2 / 400);
    bin = -2 : 0.02 : 2;
    ratio = hist(log(h./w), bin);
    ratio = conv(ratio, filter, 'same');
    [~, idx] = max(ratio);
    ratio = exp(bin(idx));
    
    %pick 20 percentage area
    areas = sort(h .* w);
    area = areas(floor(length(areas) * 0.2));
    %make sure the area fall in a suitable range
    area = max(min(area, 5000), 3000);
    
    %dimension 
        w = sqrt(area / ratio);
        h = w * ratio;
        size = [round(h / para.blockSize) round(w / para.blockSize) 31];
end
    component.b = 0;
    component.rootFilter = zeros(size);
    component.numParts = 0;
    component.parts = [];
    model.numComponent = 1;
    model.components(1) = component;
    model = calcVectorLength(model);
    model = trainLSVM(para, model, pos, neg, true, true);
end