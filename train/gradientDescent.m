function beta = gradientDescent(para, beta, X, Y, lowerbound)
%% gradient descent to find the local minima using training data X & Y
% minimize function:
%   0.5 ||beta||^2 + C \sum_i max(0, 1 - y_i ( max_(i,v) beta * v ))
%%
% para: parameters
% beta: weight
% X: training data, one data per row
% Y: label: first column (1/0) indicates positive/negative; second column
% is the index within all positive/negative examples.
% lowerbound: lowerbound of values in beta
    n = size(X, 1);
    nUnique = length(unique(Y(Y(:, 1) == 1, 2))) + length(find(Y(:, 1) == -1));
    %diff = zeros(1, para.train.numGDIter);
    for t = 1 : para.train.numGDIter
%         oldBeta = beta;
        %pick a random one
        pick = randi(n, 1, 1);
        if (Y(pick, 1) == 1)
            %there maybe multiple instances for a same positive example
            %(different latent value), find the best one
            idx = find(Y(:, 1) == 1 & Y(:, 2) == Y(pick, 2));
            scores = X(idx, :) * beta;
            [~, pick] = max(scores);
            pick = idx(pick);
        end
        value = Y(pick, 1) * (X(pick, :) * beta);
        %learning rate
        T = learningRate(para.train.learningRateType, t);
        rate = 1 / T;
        C = nUnique * para.train.C / T;
        %update beta
        beta = beta - rate * beta;
        if (value < 1)
            if (Y(pick, 1) == 1)
                C = C * para.train.J;
            else
                C = -C;
            end
            beta = beta + C * X(pick, :)';
        end
        %apply lowerbound
        beta = max(beta, lowerbound);
        %fprintf('gradient descent %d\n', t);
        %fprintf('difference %.04f\n', sum((oldBeta(:) - beta(:)) .^ 2));
%         diff(t) = sum((oldBeta(:) - beta(:)) .^ 2);
    end
%      plot(diff);
%      pause
end

function denominator = learningRate(type, t)
    switch (type)
        case 1, denominator = t + 1000;
    end
end