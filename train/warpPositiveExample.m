function latVars = warpPositiveExample(pos, model, para, maxSize)
%% warp ground truth as positive example
% usually model has only one component in this situation

    n = min(floor(maxSize / 2), numel(pos));
    [rh, rw, l] = size(model.components(1).rootFilter);
    latVars = repmat(struct('label', 1, 'idx', 0, 'feat', []), n * 2, 1);
    for i = 1 : n
        %latVars(i).label = 1;
        latVars(i).idx = i;
        feat = warpFeature(pos(i), para, [rh, rw]);
        latVars(i).feat = [reshape(feat, [1, rh * rw * l]) 1];  %last 1 is for bias term
    end
    %flip
    for i = 1 : n
            example = pos(i);
            example.flip = true;
            feat = warpFeature(example, para, [rh, rw]);
            latVars(i + n).feat = [reshape(feat, [1, rh * rw * l]) 1];  %last 1 is for bias term
    end
end