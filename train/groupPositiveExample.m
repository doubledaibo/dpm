function posGroups = groupPositiveExample(pos, n)
%% split all positive examples into n groups according to their aspects ratios.
%%
% pos: all positive examples
% n: number of groups

gt = [pos.gtBB];
idx = 3 : 4 : numel(gt);
w = gt(idx);
idx = idx + 1;
h = gt(idx);
    ratios = h ./ w;
    [ratios, I] = sort(ratios);
    m = length(ratios);
    interval = m / n;
    posGroups = cell(n, 1);
    for i = 1 : n
        posGroups{i} = pos(I( ceil((i - 1) * interval) + 1 : min(m, ceil(i * interval))));
    end
end