function model = parseModel(oldModel, beta)
%% parse beta into the dpm model
% oldModel: old model
% beta: learned beta
    model = oldModel; offset = 0;
    for i = 1 : oldModel.numComponent
        % root filter
        [height, width, len] = size(oldModel.components(i).rootFilter);
        model.components(i).rootFilter = reshape(beta(offset + 1 : offset + height * width * len), [height, width, len]);
        offset = offset + height * width * len;
        component = oldModel.components(i);
        % deformation coefficient
        for j = 1 : component.numParts
            model.components(i).parts(j).d = beta(offset + 1 : offset + 4);
            offset = offset + 4;
        end
        % part filters
        j = 1;
        while (j <= component.numParts)
            part = component.parts(j);
            if (part.partner == 1)
                %have a symmetric partner
                [height, width, len] = size(part.filter);
                filter = reshape(beta(offset + 1 : offset + height * width * len), [height, width, len]);
                model.components(i).parts(j).filter = filter;
                model.components(i).parts(j + 1).filter = flipFeat(filter);
                offset = offset + height * width * len;
                j = j + 2;
            else
                %self-symmetric
                [height, width, len] = size(part.filter);
                width2 = floor(width / 2);
                width = ceil(width / 2);
                filter = reshape(beta(offset + 1 : offset + height * width * len), [height, width, len]);
                model.components(i).parts(j).filter(:, 1 : width, :) = filter;
                model.components(i).parts(j).filter(:, width + 1 : end, :) = flipFeat(filter(:, 1 : width2, :));
                offset = offset + height * width * len;
                j = j + 1;
            end
        end
        model.components(i).b = beta(offset + 1);
        offset = offset + 1;
    end
end