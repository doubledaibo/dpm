function model = trainLSVM(para, model, pos, neg, warp, random)
%% train a Latent SVM
%%
% para: parameter
% model: init model
% pos: positive examples
% neg: negative examples
% warp: indicate whether use warped ground-truth as positive examples
% random: indicate whether use random negative examples
    
%%

    numNeg = numel(neg);
    negIdx = 0;
    latVars = [];
    numLatVars = 0;
    for i = 1 : para.train.numIter
        fprintf('LSVM %d run\n', i);
        fprintf('relabel positive examples\n');
        if (warp)
            % use warped ground-truth as positive examples
            latVars = [latVars; warpPositiveExample(pos, model, para, para.train.maxTrainSize - numLatVars)];
            numLatVars = numel(latVars);
        else
            % get latent label of each positive example    
            latVars = [latVars; relabelPositiveExample(pos, model, para, para.train.maxTrainSize - numLatVars)];
            numLatVars = numel(latVars);
        end
        fprintf('mine hard examples\n');
        % mine hard negative examples
        for j = 1 : para.train.numMineIter
            fprintf('mining %d run\n', j);
            if (random)
                % mine random negative examples
                negLatVars = mineRandomNegativeExample(neg, model, para, para.train.maxTrainSize - numLatVars);
                latVars = [latVars; negLatVars];
                numLatVars = numLatVars + numel(negLatVars);
            else
                 % mine hard negative examples using current model
                for k = 1 : numNeg
                    negIdx = negIdx + 1; if (negIdx > numNeg) negIdx = 1; end
                    negLatVars = mineHardNegativeExample(neg(negIdx), model, para, para.train.maxTrainSize - numLatVars, negIdx);
                    latVars = [latVars; negLatVars];
                    numLatVars = numLatVars + numel(negLatVars);
                    if (numLatVars == para.train.maxTrainSize)
                        break;
                    end
                end
            end
            fprintf('learning\n');
            % update model using existing hard examples
            [model, scores] = learn(model, para, latVars, pos, neg);
            % remove easy negative examples
            fprintf('removing easy examples\n');
            [latVars, numLatVars] = removeEasyExample(latVars, numLatVars, scores);
        end
    end
end

