function feat = flipFeat(oldFeat)
%% flip a 3d matrix
% work as fliplr(A(:,:,i)) for ith 2-d matrix
    feat = oldFeat(:, end : -1 : 1, :);
%     feat = oldFeat;
%     n = size(oldFeat, 3);
%     for i = 1 : n
%         feat(:,:,i) = fliplr(oldFeat(:,:,i));
%     end
end