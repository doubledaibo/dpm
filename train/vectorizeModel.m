function w = vectorizeModel(model)
%% vectorize a model
    w = [];
    for i = 1 : model.numComponent
        % root filter
        [height, width, len] = size(model.components(i).rootFilter);
        wComponent = reshape(model.components(i).rootFilter, [height * width * len, 1]);
        %deformation coefficient
        for j = 1 : model.components(i).numParts
            wComponent = [wComponent; model.components(i).parts(j).d];
        end
        % part filters
        j = 1; 
        while (j <= model.components(i).numParts)
            part = model.components(i).parts(j);
            if (part.partner == 1)
                %have a symmetric partner
                [height, width, len] = size(part.filter);
                wComponent = [wComponent; reshape(part.filter, [height * width * len, 1])];
                j = j + 1;
            else
                %self-symmetric
                [height, width, len] = size(part.filter);
                width = ceil(width / 2);
                wComponent = [wComponent; reshape(part.filter(:, 1 : width, :), [height * width * len 1])];
                j = j + 1;
            end
        end
        %bias term
        wComponent = [wComponent; model.components(i).b];
        
        w = [w; wComponent];
    end
end