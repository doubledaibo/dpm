function vecFeat = vectorizeFeature(model, instance, det, para)
%% vectorize features of positive and negative examples
    
    len = sum([model.components.vectorLength]);
    vecFeat = zeros(1, len);
    [feats, ~] = getFeatPyramid(instance, para);
    component = model.components(det.componentIdx);
    [pady, padx, ~] = size(component.rootFilter);
    padRootMap = padarray(feats{det.scaleIdx}, [pady, padx]);
    padPartMap = padarray(feats{det.scaleIdx - para.numLevels}, [pady, padx] * 2);
    offset = sum([model.components(1 : det.componentIdx - 1).vectorLength]);
    %root feature
    [feat, l] = getFeat(padRootMap, det.rawBB);
    vecFeat(offset + 1 : offset + l) = reshape(feat, [1 l]); 
    offset = offset + l;
    %deformation
    for j = 1 : component.numParts
        anchor = det.rawBB([1 2]) * 2 + component.parts(j).v';
        diff = abs(det.partBBs(j).rawBB([1 2]) - anchor);
        vecFeat(offset + 1 : offset + 4) = [diff diff .^ 2];
        offset = offset + 4;
    end
    %part feature
    j = 1;
    while (j <= component.numParts)
        if (component.parts(j).partner == 1)
            %have a symmetric partner
            [feat1, l] = getFeat(padPartMap, det.partBBs(j + 1).rawBB);
            [feat2, ~] = getFeat(padPartMap, det.partBBs(j).rawBB);
            vecFeat(offset + 1 : offset + l) = reshape(flipFeat(feat1) + feat2, [1 l]); 
            offset = offset + l;
            j = j + 2;
        else
            %self-symmetric
            [feat, ~] = getFeat(padPartMap, det.partBBs(j).rawBB);
            [~, w1, ~] = size(feat);
            w2 = floor(w1 / 2);
            w1 = ceil(w1 / 2);
            feat1 = feat(:, 1 : w1, :); 
            feat = flipFeat(feat);
            feat2 = feat(:, 1 : w2, :);
            l = numel(feat2);
            vecFeat(offset + 1 : offset + l) = reshape(feat2, [1 l]);
            l = numel(feat1);
            vecFeat(offset + 1 : offset + l) = vecFeat(offset + 1 : offset + l) + reshape(feat1, [1 l]);
            offset = offset + l;
            j = j + 1;
        end
    end
    %bias term
    vecFeat(offset + 1) = 1;
end

function [feat, len] = getFeat(map, bb)
    feat = map(bb(2) : bb(2) + bb(4) - 1, bb(1) : bb(1) + bb(3) - 1, :);
    len = bb(3) * bb(4) * size(map, 3);
end