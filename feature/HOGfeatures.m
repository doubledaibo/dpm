function F = HOGfeatures(im, bin)
   %% generate HOG features
   %based on DPM paper's code
   %translate into matlab, originally C
   %% 
   %avoid division by zero
   eps = 0.0001;
   % unit vectors used to compute gradient orientation
   uu = [1.0000, ...
         0.9397, ...
         0.7660, ...
         0.500, ...
         0.1736, ...
         -0.1736, ...
         -0.500, ...
         -0.7660, ...
         -0.9397];
   vv = [0.0000, ...
         0.3420, ...
         0.6428, ...
         0.8660, ...
         0.9848, ...
         0.9848, ...
         0.8660, ...
         0.6428, ...
         0.3420];
   uv = [uu; vv];
   dims = size(im);
   if (length(dims) ~= 3 || dims(3) ~= 3)
       error('invalid input');
   end
   blocks = [round(dims(1) / bin), round(dims(2) / bin)];
   hist = zeros(blocks(1), blocks(2), 18);
   out1 = max(blocks(1) - 2, 0);
   out2 = max(blocks(2) - 2, 0);
   F = zeros(out1, out2, 27 + 4);
   out = [out1 out2];
   visible = blocks * bin;
   for x = 2 : visible(2) - 1
       for y = 2 : visible(1) - 1
           dy = zeros(1, 3);
           dx = zeros(1, 3);
           v = zeros(1, 3);
           yy = min(y, dims(1) - 1);
           xx = min(x, dims(2) - 1);
           for k = 1 : 3
            dy(k) = im(yy + 1, xx, k) - im(yy - 1, xx, k);
            dx(k) = im(yy, xx + 1, k) - im(yy, xx - 1, k);
            v(k) = dx(k)^2 + dy(k)^2;
           end
           % pick channel with strongest gradient
           [v, idx] = max(v);
           dy = dy(idx); dx = dx(idx);
           % snap to one of 18 orientations
           dot = [dx dy] * uv;
           [~, o] = max([dot -dot]);
           % add to 4 histograms around pixels using linear interpolation
           xp = (x - 1 + 0.5) / bin - 0.5; yp = (y - 1 + 0.5) / bin - 0.5;
           ixp = floor(xp); iyp = floor(yp);
           vx0 = xp - ixp; vy0 = yp - iyp;
           vx1 = 1 - vx0; vy1 = 1 - vy0;
           ixp = 1 + ixp; iyp = 1 + iyp;
           v = sqrt(v);
           if (ixp >= 1 && iyp >= 1)
               hist(iyp, ixp, o) = hist(iyp, ixp, o) + vx1*vy1*v;
           end
           if (ixp + 1 <= blocks(2) && iyp >= 1)
               hist(iyp, ixp + 1, o) = hist(iyp, ixp + 1, o) + vx0*vy1*v;
           end
           if (ixp >= 1 && iyp + 1 <= blocks(1))
               hist(iyp + 1, ixp, o) = hist(iyp + 1, ixp, o) + vx1*vy0*v;
           end
           if (ixp + 1 <= blocks(2) && iyp + 1 <= blocks(1))
               hist(iyp + 1, ixp + 1, o) = hist(iyp + 1, ixp + 1, o) + vx0*vy0*v;
           end
       end
   end
   % compute energy in each block by summing over orientations
   norm = sum((hist(:,:,1 : 9) + hist(:, :, 10 : 18)) .^ 2, 3);
   % compute features
   normkernel = {[0 0 0; 0 1 1; 0 1 1], ...
                 [0 1 1; 0 1 1; 0 0 0], ...
                 [0 0 0; 1 1 0; 1 1 0], ...
                 [1 1 0; 1 1 0; 0 0 0]};
   n = zeros(out(1), out(2), 4);
   for kid = 1 : 4
       n(:,:,kid) = conv2(norm, rot90(normkernel{kid}, 2), 'valid');
   end
   n = 1 ./ sqrt(n + eps);
   hist = hist(2 : out(1) + 1, 2 : out(2) + 1, :);
   for o = 1 : 9
       for kid = 1 : 4
            h1 = hist(:,:,o) .* n(:,:,kid);
            h2 = hist(:,:,o + 9) .* n(:,:,kid);
            % contrast-insensitive features
            F(:,:,18 + o) = F(:,:,18 + o) + min(h1 + h2, 0.2);
            h1 = min(h1, 0.2);
            h2 = min(h2, 0.2);
            % contrast-sensitive features
            F(:,:,o) = F(:,:,o) + h1;
            F(:,:,o + 9) = F(:,:,o + 9) + h2;
            % texture features
            F(:,:,27 + kid) = F(:,:,27 + kid) + 0.2357 * 2 * (h1 + h2);
       end
   end
   F = F / 2;
   clear hist;
   clear norm;
end

