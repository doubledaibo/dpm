function dets = detect(instance, gtBB, model, para)
%% 
% detection objects on an image.
%%
% instance: instance for detection, format follows example  
% gtBB: ground-truth bounding box, used for training
% model: model for detection
% para: parameters for detection
        
    useBB = 1 - isempty(gtBB);
    [feat, scales, gtBB] = getFeatPyramid(instance, para);
    totalLevels = length(feat);
    dets = [];
    for i = 1 + para.numLevels : totalLevels
        %fprintf('detect at %d scaleLevel\n', i);
        if (useBB == 1)
            newDet = match(para, gtBB, feat{i}, feat{i - para.numLevels}, model, scales(i), i);
            if (~isempty(newDet))
                if (isempty(dets))
                    dets = newDet;
                else
                    if (newDet.score > dets.score)
                        dets = newDet;
                    end
                end
            end
        else
            dets = [dets; match(para, gtBB, feat{i}, feat{i - para.numLevels}, model, scales(i), i)];
        end
    end
    showBoxes(dets, para);
    %post-processing, e.g. non-maxima suppression
    if (numel(dets) > 1)
        dets = postProcess(para, dets);
    end
end

