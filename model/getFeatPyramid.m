function [feat, scales, gtBB] = getFeatPyramid(instance, para)
%% get feature pyramid of the instance
% if calculated, use existing featPyramid
    flip = false;
    if (isfield(instance, 'flip') && instance.flip)
        %cacheName = [cacheName '_flip'];
        flip = true;
    end
        if (instance.label == -1)
            gtBB = [];
        else
            gtBB = instance.gtBB;
        end
        fprintf('generating featurepyramid for %s\n', instance.fileName);
        im = color(imread(fullfile(instance.fullPath, [instance.fileName '.' instance.ext])));
        if (para.train.cropPos == 1 && instance.label == 1)
            [im, gtBB] = cropPositiveExample(im, instance.gtBB);
        end
        if (flip)
            im = im(:,end:-1:1,:);
            gtBB(1) = size(im, 2) - gtBB(1) + 1 - gtBB(3) + 1;
        end
        [feat, scales] = featPyramid(im, para.blockSize, para.numLevels);
        clf;
        image(im);
        axis equal;
        axis on;
end

