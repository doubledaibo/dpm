function IoU = calcIoU(bb1, bb2)
    bb1 = [bb1([2 1]) bb1([4 3]) + bb1([2 1])];
    bb2 = [bb2([2 1]) bb2([4 3]) + bb2([2 1])];
   % bb = [top, left, bottom, right]
    I = [0, 0, 0, 0];
    I(1) = max(bb1(1), bb2(1));
    I(2) = max(bb1(2), bb2(2));
    I(3) = min(bb1(3), bb2(3));
    I(4) = min(bb1(4), bb2(4));
    if (I(1) > I(3))
        IoU = 0;
    else
        sI = max(0, calcRec(I));
        sU = calcRec(bb1) + calcRec(bb2) - sI;
        IoU = sI / sU;
    end
end

function s = calcRec(bb)
    s = (bb(3) - bb(1) + 1) * (bb(4) - bb(2) + 1);
end