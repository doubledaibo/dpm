function dets = match(para, gtBB, featLowResolution, featHighResolution, model, scale, scaleIdx)
%%
% detect objects on image given specific scale
%%
% para: parameter
% gtBB: ground-truth bounding box, if is not empty, return only one highest
% detection that has siginificant overlap with the gtBB
% featLowResolution: feature map in low resolution, for detecting whole object
% featHighResolution: feature map in high resolution, for detecting parts, twice
% the scale of low resolution feature
% model: dpm model
% scale: scale of featLowResolution relative to original image
% scaleIdx: index of the scale in feature pyramid
    useBB = 1 - isempty(gtBB);
    dets = [];
    for c = 1 : model.numComponent
        component = model.components(c);
        %padding zeros for detecting partial visible objects
        [pady, padx, ~] = size(component.rootFilter);
        padfeatLowResolution = padarray(featLowResolution, [pady, padx]);
        padfeatHighResolution = padarray(featHighResolution, [pady, padx] * 2);

        [height, width, ~] = size(padfeatLowResolution);
        height = height - pady + 1;
        width = width - padx + 1;
        %score = \sum{ filter score } - \sum{ deformation cost} + bias
        %root filter score
        rootResponse = conv3d(padfeatLowResolution, component.rootFilter);
        totalResponse = rootResponse;
        %best place of each part when root is at (x,y)
        px = cell(component.numParts, 1);
        py = cell(component.numParts, 1);
        for i = 1 : component.numParts
            %ith part filter score - ith part deformation cost, details refer
            %to dpm paper
            partModel = component.parts(i);
            partResponse = conv3d(padfeatHighResolution, partModel.filter);
            %get max(partResponse(x + dx, y + dy) - d * (dx, dy, dx^2, dy^2))
            %for every x,y quickly
            v = partModel.v;
            [dt, px{i}, py{i}] = distanceTransform(partResponse, partModel.d);
            totalResponse = totalResponse + dt(v(2) + 2 : 2 : v(2) + height * 2, v(1) + 2 : 2 : v(1) + width * 2);
        end
        %bias term
        totalResponse = totalResponse + component.b;
        %keep score above threshold
        totalResponse(totalResponse <= para.detect.threshold) = 0;
        %get detections
        [y, x] = find(totalResponse ~= 0);
        numDets = length(y);
        newDets = [];
        % size information
        [rH, rW, ~] = size(component.rootFilter);
        pH = zeros(component.numParts, 1);
        pW = pH;
        for i = 1 : component.numParts
            [pH(i), pW(i), ~] = size(component.parts(i).filter);
        end
        % generate detection
        if (useBB ~= 1)
            %all detections
            for i = numDets : -1 : 1
                newDets(i, 1).rawBB = [x(i) y(i) rW rH];
                bb = [y(i) - pady x(i) - padx rH rW] / scale;
                bb([1 2]) = max(1, bb([1 2]));
                newDets(i).rootBB = bb([2 1 4 3]);
                newDets(i).score = totalResponse(y(i), x(i));
                newDets(i).numParts = component.numParts;
                partBBs = [];
                for j = 1 : newDets(i).numParts
                    v = component.parts(j).v;
                    bb = [[py{j}(y(i) * 2 + v(2), x(i) * 2 + v(1)) px{j}(y(i) * 2 + v(2), x(i) * 2 + v(1))] pH(j) pW(j)];
                    partBBs(j).rawBB = bb([2 1 4 3]);
                    bb = [[py{j}(y(i) * 2 + v(2), x(i) * 2 + v(1)) px{j}(y(i) * 2 + v(2), x(i) * 2 + v(1))] / 2 - [pady padx] [pH(j) pW(j)] / 2] / scale;
                    bb([1 2]) = max(1, bb([1 2]));
                    partBBs(j).BB = bb([2 1 4 3]);
                end
                newDets(i).partBBs = partBBs;
                newDets(i).scaleIdx = scaleIdx;
                newDets(i).scale = scale;
                newDets(i).componentIdx = c;
            end

            dets = [dets; newDets];
        else
            %find highest detection that has siginificant overlap with the gtBB
            for i = 1 : numDets
                bb = [y(i) - pady x(i) - padx rH rW] / scale;
                bb([1 2]) = max(1, bb([1 2]));
                bb = bb([2 1 4 3]);
                if (isempty(dets) || dets.score < totalResponse(y(i), x(i)))
                    if (calcIoU(bb * para.blockSize, gtBB) >= para.train.overlapThres)
                        dets.rawBB = [x(i) y(i) rW rH];
                        dets.rootBB = bb;
                        dets.score = totalResponse(y(i), x(i));
                        dets.numParts = component.numParts;
                        partBBs = [];
                        for j = 1 : dets.numParts
                            v = component.parts(j).v;
                            bb = [[py{j}(y(i) * 2 + v(2), x(i) * 2 + v(1)) px{j}(y(i) * 2 + v(2), x(i) * 2 + v(1))] pH(j) pW(j)];
                            partBBs(j).rawBB = bb([2 1 4 3]);
                            bb = [[py{j}(y(i) * 2 + v(2), x(i) * 2 + v(1)) px{j}(y(i) * 2 + v(2), x(i) * 2 + v(1))] / 2 - [pady padx] [pH(j) pW(j)] / 2] / scale;
                            bb([1 2]) = max(1, bb([1 2]));
                            partBBs(j).BB = bb([2 1 4 3]);
                        end
                        dets.partBBs = partBBs;
                        dets.scaleIdx = scaleIdx;
                        dets.scale = scale;
                        dets.componentIdx = c;
                    end
                end
            end
        end
    end
end

