function feat = warpFeature(instance, para, realSize)
    targetSize = (realSize + 2) * para.blockSize;
    im = color(imread(fullfile(instance.fullPath, [instance.fileName '.' instance.ext])));
    bb = instance.gtBB;
    if (isfield(instance, 'flip') && instance.flip)
        im = im(:,end:-1:1,:);
        bb(1) = size(im, 2) - bb(1) + 1 - bb(3) + 1;
    end
    padx = bb(3) / realSize(2);
    pady = bb(4) / realSize(1);
    x1 = round(bb(1)-padx);
    x2 = round(bb(1) + bb(3) - 1 + padx);
    y1 = round(bb(2)-pady);
    y2 = round(bb(2) + bb(4) - 1 + pady);
    window = subarray(im, y1, y2, x1, x2, 1);
    imregion = imresize(window, targetSize, 'bilinear');
    feat = features(imregion, para.blockSize);
    image(imregion / 256);
    drawnow;
end