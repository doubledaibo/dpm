function dets = postProcess(para, rowDets)
%post-processing, e.g. non-maxima suppression

    %% non-maxima suppression
    n = numel(rowDets);
    if (n == 0)
        dets = [];
        return;
    end
    score = [rowDets.score];
    [~, idx] = sort(score, 2, 'descend');
    m = 0;
    for i = 1 : n
        keep = true;
        for j = 1 : m
            if (calcIoU(rowDets(idx(i)).rootBB, dets(j).rootBB) >= para.postprocess.NMSthres)
                keep = false;
                break;
            end
        end
        if (keep)
            m = m + 1;
            dets(m, 1) = rowDets(idx(i));
        end
    end
end