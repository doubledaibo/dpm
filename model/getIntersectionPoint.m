function p = getIntersectionPoint(f1, f2, x1, x2, a, b)
% calculate intersection of functions 
% a(x - x1)^2 + b||x - x1|| + f1 
% a(x - x2)^2 + b||x - x2|| + f2
% x1 < x2
p1 = (f2 + x2^2 * a + x2 * b - f1 - x1^2 * a + x1 * b) ...
      / ...
     (2 * (x2 * a - x1 * a + b));
p2 = (f2 + x2^2 * a + x2 * b - f1 - x1^2 * a - x1 * b) ...
      / ...
     (2 * (x2 * a - x1 * a));
if (p1 < x1)
    p = p2;
else
    p = p1;
end
end