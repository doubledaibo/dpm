function response = conv3d(A, B)
%% convolution of 3d matrices A & B
% 3 dimensional length of A and B is same.
% response is a 2-d matrix.

    [h1, w1, n] = size(A);
    [h2, w2, ~] = size(B);
    response = zeros(h1 - h2 + 1, w1 - w2 + 1);
    for i = 1 : n
        singleResponse = conv2(squeeze(A(:,:,i)), rot90(squeeze(B(:,:,i)), 2), 'valid');
        response = singleResponse + response;
    end
end

