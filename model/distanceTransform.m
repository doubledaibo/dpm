function [dt, px, py] = distanceTransform(response, d)
%get max(partResponse(x + dx, y + dy) - d * (dx, dy, dx^2, dy^2))
%for every x,y quickly
% fix row first and apply 1-d distance transform twice
    response = - response;
    [height, width] = size(response);
    rowDT = zeros(height, width);
    dt = zeros(height, width);
    py = zeros(height, width);
    rowPx = zeros(height, width);
    px = zeros(height, width);
    t = zeros(width + 1, 1);
    v = zeros(width, 1);
    for i = 1 : height
        %row-wise
        t(1) = -Inf; v(1) = 1; t(2) = Inf; k = 1;
        for j = 2 : width
            tCur = getIntersectionPoint(response(i, j), response(i, v(k)), j, v(k), d(3), d(1));
            while (tCur <= t(k))
                k = k - 1;
                tCur = getIntersectionPoint(response(i, j), response(i, v(k)), j, v(k), d(3), d(1));
            end
            k = k + 1; t(k) = tCur; v(k) = j; t(k + 1) = Inf;
        end
        k = 1;
        for j = 1 : width
            while (t(k + 1) < j)
                k = k + 1;
            end
            rowDT(i, j) = response(i, v(k)) + d(1) * abs(j - v(k)) + d(3) * (j - v(k))^2;
            rowPx(i, j) = v(k);
        end
    end
    %column-wise
    t = zeros(height + 1, 1);
    v = zeros(height, 1);
    for j = 1 : width
        t(1) = -Inf; v(1) = 1; t(2) = Inf; k = 1;
        for i = 2 : height
            tCur = getIntersectionPoint(rowDT(i, j), rowDT(v(k), j), i, v(k), d(4), d(2));
            while (tCur <= t(k))
                k = k - 1;
                tCur = getIntersectionPoint(rowDT(i, j), rowDT(v(k), j), i, v(k), d(4), d(2));
            end
            k = k + 1; t(k) = tCur; v(k) = i; t(k + 1) = Inf; 
        end
        k = 1; 
        for i = 1 : height
            while (t(k + 1) < i)
                k = k + 1;
            end
            dt(i, j) = rowDT(v(k), j) + d(2) * abs(i - v(k)) + d(4) * (i - v(k))^2;
            px(i, j) = rowPx(v(k), j);
            py(i, j) = v(k);
        end
    end
    dt = -dt;
end

