function [dt, px, py] = bruteforceDistanceTransform(response, d)
%brute force method to find max(partResponse(x + dx, y + dy) - d * (dx, dy, dx^2, dy^2))
%for every x,y 
    [height, width] = size(response);
    dt = zeros(height, width);
    px = zeros(height, width);
    py = zeros(height, width);
    for i = 1 : height
        for j = 1 : width
            dt(i,j) = -Inf;
            for h = 1 : height
                for w = 1 : width
                    sum = response(h,w) - [abs(j - w) abs(i - h) (j - w)^2 (i - h)^2] * d;
                    if (sum > dt(i,j))
                        dt(i,j) = sum;
                        px(i,j) = w;
                        py(i,j) = h;
                    end
                end
            end
        end
    end
end

