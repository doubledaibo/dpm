function showBoxes(dets, para)
    n = numel(dets);
    for i = 1 : n
        rectangle('Position', dets(i).rootBB * para.blockSize, 'EdgeColor', 'r');
        for j = 1 : dets(i).numParts
            rectangle('Position', dets(i).partBBs(j).BB * para.blockSize, 'EdgeColor', 'b');
        end
    end
    drawnow;
    pause(0.1);
end

