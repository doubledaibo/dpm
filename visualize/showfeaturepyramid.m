function showfeaturepyramid(pos)
	n = numel(pos);
	for i = 1 : n
	    [feat, scales] = getFeatPyramid(pos(i), para);
	    im = double(imread(fullfile(pos(i).fullPath, [pos(i).fileName '.' pos(i).ext])));
	    m = length(feat);
	    for j = 1 : m
		imr = imresize(im, scales(i), 'box');
		subplot(1, 2, 1), imshow(imr / 256);
		subplot(1, 2, 2), imshow(visualizeHOG(feat{j}));
		pause
	    end
	end
end
