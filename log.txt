bug 1: beta doesn't contain deformation coefficient
    fixed

bug 2: not apply lowerbound constraints   
    fixed

bug 3: need compute vectorLength in initial model
    fixed

bug 4: need change detect using mixture model
    fixed 

bug 5: need change expression such as dets(:).score to [dets.score]
    fixed
    
bug 6: not use anchor information of each part
    fixed

bug 7: not add bias term in vectorization
    fixed

bug 8: bounding box may exceed boundaries (due to padding)
    fixed

bug 9: feature is 1-d array not a value, feature map is 3-dimension, so as filter
    fixed 

bug 10: need use convn or write an advanced version of conv2
    fixed 
bug 11: not care of maxTrainSize when generating positive examples
    fixed

modification 1: add .feat for latVars
    done