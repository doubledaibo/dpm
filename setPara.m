para.numLevels = 5;
para.blockSize = 8;
para.numParts = 6;
%train

%init
init.numGroup = 2;
init.initNegSize = 300;

train.init = init;
train.numIter = 3;
train.numMineIter = 6;
train.numGDIter = 100000;
train.learningRateType = 1;
train.maxTrainSize = 10000;
train.overlapThres = 0.7;
train.C = 0.6;
train.J = 1;
train.filterLowerbound = -100;
train.deformLowerbound = 0.01;
train.biasLowerbound = -100;
train.cropPos = 1;

para.train = train;

%detect
para.detect.threshold = 0;

%postprocess
para.postprocess.NMSthres = 0.5;

