% % % clc, clear;
% % % class = 'person';
% % % addpath(genpath('.'));
% % % load(['voc_' class]);

setPara;
clear train, init;

posGroups = groupPositiveExample(pos, para.train.init.numGroup);
n = numel(posGroups);
models = cell(n, 1);
for i = 1 : n
fprintf('initializing %d th component\n', i);
para.train.numIter = 1;
para.train.numMineIter = 1;
models{i} = initModel(para, posGroups{i}, neg);
end
%merge into one mixture model
model.numComponent = n;
for i = model.numComponent : -1 : 1
model.components(i, 1) = models{i}.components(1);
end
save('initModel.mat', 'model');

n = model.numComponent;
fprintf('train root filter using SVM\n');
para.train.numIter = 2;
para.train.numMineIter = 2;
model = trainLSVM(para, model, pos, neg(1 : para.train.init.initNegSize), false, false);
save('initLSVMModel.mat', 'model');

for i = 1 : n
        fprintf('add parts for %d th component\n', i);
        model.components(i) = addParts(model.components(i), para);
end
model = calcVectorLength(model);
save('initaddpartmodel.mat', 'model');

para.train.numIter = 1;
para.train.numMineIter = 4;
model = trainLSVM(para, model, pos, neg(1 : para.train.init.initNegSize), false, false);
save('finalmodel1round.mat', 'model');

para.train.numIter = 6;
para.train.numMineIter = 2;
model = trainLSVM(para, model, pos, neg(1 : para.train.init.initNegSize), false, false);
save('finalmodel2round.mat', 'model');

para.train.numIter = 1;
para.train.numMineIter = 3;
model = trainLSVM(para, model, pos, neg, false, false);
save('finalmodel3round.mat', 'model');


